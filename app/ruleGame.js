  //Rule

 const wrapRule = document.createElement('div');
 const ruleField = document.createElement('div');
 const crossRule = document.createElement('span');
 const titleRuleField = document.createElement('h1');

 const wrapCloverRule = document.createElement('div');
 const cloverRuleImg = document.createElement('img');
 const betCloverRule = document.createElement('span');

 const wrapCardRule = document.createElement('div');
 const cardRuleImg = document.createElement('img');
 const betCardRule = document.createElement('span');

 const wrapChipRule = document.createElement('div');
 const chipRuleImg = document.createElement('img');
 const betChipRule = document.createElement('span');

 const wrapHorseshoeRule = document.createElement('div');
 const horseshoeRuleImg = document.createElement('img');
 const betHorseshoeRule = document.createElement('span');

 const wrapDiamondRule = document.createElement('div');
 const diamondRuleImg = document.createElement('img');
 const betDiamondRule = document.createElement('span');

 const wrapSevenRule = document.createElement('div');
 const sevenRuleImg = document.createElement('img');
 const betSevenRule = document.createElement('span');

 //Rule abilities

 wrapRule.classList.add('wrapRule');
 ruleField.classList.add('ruleField');
 crossRule.classList.add('crossRule');
 crossRule.innerHTML = '&times';
 titleRuleField.classList.add('titleRuleField');
 titleRuleField.innerText = 'При выпадении: ';

 wrapCloverRule.classList.add('wrapCloverRule');
 cloverRuleImg.setAttribute('src','img/ruleClover.png');
 cloverRuleImg.classList.add('ruleImg')
 betCloverRule.innerHTML = 'ставка &times 1';
 betCloverRule.classList.add('betCloverRule','betRule');

 wrapCardRule.classList.add('wrapCardRule');
 cardRuleImg.setAttribute('src','img/ruleCard.png');
 cardRuleImg.classList.add('ruleImg');
 betCardRule.innerHTML = 'ставка &times 2';
 betCardRule.classList.add('betCardRule','betRule');

 wrapChipRule.classList.add('wrapChipRule');
 chipRuleImg.setAttribute('src','img/ruleChip.png');
 chipRuleImg.classList.add('ruleImg')
 betChipRule.innerHTML = 'ставка &times 3';
 betChipRule.classList.add('betChipRule','betRule');

 wrapHorseshoeRule.classList.add('wrapHorseshoeRule');
 horseshoeRuleImg.setAttribute('src','img/ruleHorseshoe.png');
 horseshoeRuleImg.classList.add('ruleImg');
 betHorseshoeRule.innerHTML = 'ставка &times 4';
 betHorseshoeRule.classList.add('betHorseshoeRule','betRule');

 wrapDiamondRule.classList.add('wrapDiamondRule');
 diamondRuleImg.setAttribute('src','img/ruleDiamond.png');
 diamondRuleImg.classList.add('ruleImg');
 betDiamondRule.innerHTML = 'ставка &times 5';
 betDiamondRule.classList.add('betDiamondRule','betRule');

 wrapSevenRule.classList.add('wrapSevenRule');
 sevenRuleImg.setAttribute('src','img/ruleSeven.png');
 sevenRuleImg.classList.add('ruleImg');
 betSevenRule.innerHTML = 'ставка &times 6';
 betSevenRule.classList.add('betSevenRule','betRule');

 export {wrapRule,ruleField,crossRule,titleRuleField,wrapCloverRule,cloverRuleImg,betCloverRule,wrapCardRule,cardRuleImg,betCardRule,wrapChipRule,chipRuleImg};
 export {betChipRule,wrapHorseshoeRule,horseshoeRuleImg,betHorseshoeRule,wrapDiamondRule,diamondRuleImg,betDiamondRule,wrapSevenRule,sevenRuleImg,betSevenRule};
