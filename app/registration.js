//Registration

const wrapFormReg = document.createElement('div');
const formReg = document.createElement('form');
const crossForm = document.createElement('span');
const titleformReg = document.createElement('h1');
const loginReg = document.createElement('input');

const passwordReg = document.createElement('input');
const wrapPassword = document.createElement('div');
const eyeButton = document.createElement('a');

const wrapPasswordRepeat = document.createElement('div');
const repeatPasswordReg = document.createElement('input');
const eyeButtonRepeat = document.createElement('a');

const mesPas = document.createElement('span');
const inputFormReg = document.createElement('input');
const recetFormReg = document.createElement('button');

// Registration abilities

wrapFormReg.classList.add('wrapFormReg');
formReg.classList.add('formRegMain');
crossForm.classList.add('crossForm','crossFormEnt');
crossForm.innerHTML = '&times';
titleformReg.innerText = 'Регистрация';

loginReg.setAttribute('type','text');
loginReg.setAttribute('placeholder','Введите логин');
loginReg.classList.add('inputForm','controlLogin');
loginReg.setAttribute('required','true');

wrapPassword.classList.add('wrapPassword');
passwordReg.setAttribute('type','password');
passwordReg.setAttribute('placeholder','Введите пароль');
passwordReg.classList.add('inputForm','pas1');
passwordReg.setAttribute('required','true');
eyeButton.classList.add('eyeButton');

wrapPasswordRepeat.classList.add('wrapPasswordRepeat');
repeatPasswordReg.classList.add('inputForm', 'pas2');
repeatPasswordReg.setAttribute('type','password');
repeatPasswordReg.setAttribute('placeholder','Повторите пароль');
repeatPasswordReg.setAttribute('required','true');
eyeButtonRepeat.classList.add('eyeButtonRepeat');
mesPas.innerHTML = 'Пароли не совпадают';
mesPas.classList.add('mesPas');

inputFormReg.setAttribute('type','submit');
inputFormReg.value = 'Зарегистрироваться';
inputFormReg.classList.add('inputFormReg');

recetFormReg.setAttribute('type','reset');
recetFormReg.innerText = 'Очистить';
recetFormReg.classList.add('recetFormReg');

//Eyes registration

export const eyeWork = () => {
	if (passwordReg.getAttribute('type') == 'password') {
	   eyeButton.style.backgroundImage = 'url(img/iconsEye.png)';
	   passwordReg.setAttribute('type','text');
	}
	else {
	   eyeButton.style.backgroundImage = 'url(img/iconsInvisibleEye.png)';
	   passwordReg.setAttribute('type','password');
	}
};

export const eyeWorkRepeat = () => {
	if (repeatPasswordReg.getAttribute('type') == 'password') {
	   eyeButtonRepeat.style.backgroundImage = 'url(img/iconsEye.png)';
	   repeatPasswordReg.setAttribute('type','text');
	}
	else {
	   eyeButtonRepeat.style.backgroundImage = 'url(img/iconsInvisibleEye.png)';
	   repeatPasswordReg.setAttribute('type','password');
	}
};

//Control login registration

const errorLogLength = document.createElement('span');
errorLogLength.classList.add('errorLogLength');
errorLogLength.innerHTML = 'Поле должно содержать от 5 до 20 букв или цифр';

const errorSimilarLogin = document.createElement('span');
errorSimilarLogin.classList.add('errorSimilarLogin');
errorSimilarLogin.innerHTML = 'Такой логин уже зарегистрирован';

export const controlLoginLength = () => {
	const controlLogin = document.querySelector('.controlLogin').value;
		if((controlLogin.length < 5||controlLogin.length > 15)||(/^[a-zA-Z1-яА-Я0-9]+$/.test(controlLogin) === false)){
			errorLogLength.style.display = 'block';
			loginReg.style.border = '1px solid red';
			inputFormReg.setAttribute('disabled','true');
            const controlSimilarLogins = () => {
				if(localStorage.hasOwnProperty(loginReg.value)){
                   errorSimilarLogin.style.display = 'block';
                   loginReg.style.border = '1px solid red';
                   inputFormReg.setAttribute('disabled','true');
                }
                else{
                   errorSimilarLogin.style.display = 'none';
                }
            }
            formReg.append(errorSimilarLogin)

            formReg.addEventListener('keyup',controlSimilarLogins);
		}
		else{
			errorLogLength.style.display = 'none';
			loginReg.style.border = 'none';
			inputFormReg.removeAttribute('disabled','true');
		}
};

//Control password registration

const errorPasLength = document.createElement('span');
errorPasLength.classList.add('errorPasLength');
errorPasLength.innerHTML = 'Введите от 5 до 20 букв или цифр';

export const controlPasLength = () => {
	const pas1 = document.querySelector('.pas1').value;
	if ((pas1.length<5||pas1.length>20)||(/^[a-zA-Z1-яА-Я0-9]+$/.test(pas1) === false)){
		errorPasLength.style.display = 'block';
		passwordReg.style.border = '1px solid red';
		inputFormReg.setAttribute('disabled','true');
		const controlPassword = e => {
			const pas1 = document.querySelector('.pas1').value;
			const pas2 = document.querySelector('.pas2').value;
			for(let i=0;i < pas2.length; i++){
				if(pas1[i] != pas2[i] && e.keyCode != 8){
					mesPas.style.display = 'block';
					repeatPasswordReg.style.border = '1px solid red';
					inputFormReg.setAttribute('disabled','true');
					break;
				}
				if(pas1[i] === pas2[i]&& e.keyCode == 8){
					mesPas.style.display = 'none';
					repeatPasswordReg.style.border = 'none';
					inputFormReg.removeAttribute('disabled','true');
					break;
				}
			}
		};
		repeatPasswordReg.addEventListener('keyup',controlPassword);
	}
	else{
		errorPasLength.style.display = 'none';
		passwordReg.style.border = 'none';
		inputFormReg.removeAttribute('disabled','true');
	}
};
export const controlPasLast = () => {
	const pas1 = document.querySelector('.pas1').value;
	const pas2 = document.querySelector('.pas2').value;
	if(pas1.length!= pas2.length){
		mesPas.style.display = 'block';
		repeatPasswordReg.style.border = '1px solid red';
		inputFormReg.setAttribute('disabled','true');
	}
	else{
		mesPas.style.display = 'none';
		repeatPasswordReg.style.border = 'none';
		inputFormReg.removeAttribute('disabled','true');
	}
};

export {wrapFormReg,formReg,crossForm,titleformReg,loginReg,passwordReg,wrapPassword,eyeButton,wrapPasswordRepeat,repeatPasswordReg,eyeButtonRepeat,mesPas,inputFormReg,recetFormReg};
export {errorLogLength,errorSimilarLogin,errorPasLength};