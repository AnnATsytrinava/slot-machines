//Header

const headerElement = document.createElement('header');
const insideHeader = document.createElement('div');
const logo = document.createElement('img');
const navigationHeader = document.createElement('nav');
const inputEnter = document.createElement('input');
const inputRegistration = document.createElement('input');

insideHeader.setAttribute('class','insideHeader');
logo.setAttribute('src','img/logo.png');
navigationHeader.setAttribute('class','navHeader');

inputEnter.setAttribute('type','button');
inputEnter.value = 'Войти';
inputEnter.classList.add('buttonHeader', 'buttonHeaderEnter');

inputRegistration.setAttribute('type','button');
inputRegistration.value = 'Регистрация';
inputRegistration.classList.add('buttonHeader','inputRegistration');

export {headerElement,insideHeader,logo,navigationHeader,inputEnter,inputRegistration};
