//Create field game

const wrapperGame = document.createElement('div');
const gameField = document.createElement('div');
const wrapAllRings = document.createElement('div');

wrapperGame.classList.add('wrapperGame');
gameField.classList.add('gameField');
wrapAllRings.classList.add('wrapAllRings');

//Create game's buttons

const wrapButtonGame = document.createElement('div');
const wrapBets = document.createElement('div');
const titleBet = document.createElement('span');
const bet50 = document.createElement('input');
const bet100 = document.createElement('input');
const wrapBalance = document.createElement('div');
const titleBalance = document.createElement('span');
const balanceBtn = document.createElement('input');

wrapButtonGame.classList.add('wrapButtonGame');
wrapBets.classList.add('wrapBets');
titleBet.classList.add('titleBet');

bet50.classList.add('bet50','bet');
bet100.classList.add('bet100');
bet50.setAttribute('type','button');
bet100.setAttribute('type','button');
bet50.value = '50';
bet100.value = '100';

wrapBalance.classList.add('wrapBalance');
titleBalance.classList.add('titleBalance');
balanceBtn.classList.add('balanceBtn');
titleBet.innerHTML = 'Ставка';
balanceBtn.setAttribute('type','button');
titleBalance.innerHTML = 'Баланс';
balanceBtn.value = '500';

// Ring 1
			
const wrapRing1 = document.createElement('div');
const ring1 = document.createElement('div');
const diamond1 = document.createElement('div');
const clover1 = document.createElement('div');
const seven1 = document.createElement('div');
const horseshoe1 = document.createElement('div');
const chip1 = document.createElement('div');
const card1 = document.createElement('div');

wrapRing1.classList.add('wrapRing1');
ring1.classList.add('ring1');
diamond1.classList.add('ring1Slot', 'diamond');
clover1.classList.add('ring1Slot', 'clover');
seven1.classList.add('ring1Slot', 'seven');
horseshoe1.classList.add('ring1Slot', 'horseshoe');
chip1.classList.add('ring1Slot', 'chip');
card1.classList.add('ring1Slot', 'card');

 //Ring 2

 const wrapRing2 = document.createElement('div');
 const ring2 = document.createElement('div');
 const diamond2 = document.createElement('div');
 const clover2 = document.createElement('div');
 const seven2 = document.createElement('div');
 const horseshoe2 = document.createElement('div');
 const chip2 = document.createElement('div');
 const card2 = document.createElement('div');

 wrapRing2.classList.add('wrapRing2');
 ring2.classList.add('ring2');
 diamond2.classList.add('ring2Slot', 'diamond');
 clover2.classList.add('ring2Slot', 'clover');
 seven2.classList.add('ring2Slot', 'seven');
 horseshoe2.classList.add('ring2Slot', 'horseshoe');
 chip2.classList.add('ring2Slot', 'chip');
 card2.classList.add('ring2Slot', 'card');

//Ring 3

const wrapRing3 = document.createElement('div');
const ring3 = document.createElement('div');
const diamond3 = document.createElement('div');
const clover3 = document.createElement('div');
const seven3 = document.createElement('div');
const horseshoe3 = document.createElement('div');
const chip3 = document.createElement('div');
const card3 = document.createElement('div');

wrapRing3.classList.add('wrapRing3');
ring3.classList.add('ring3');
diamond3.classList.add('ring3Slot', 'diamond');
clover3.classList.add('ring3Slot', 'clover');
seven3.classList.add('ring3Slot', 'seven');
horseshoe3.classList.add('ring3Slot', 'horseshoe');
chip3.classList.add('ring3Slot', 'chip');
card3.classList.add('ring3Slot', 'card');

const ringOptions = document.createElement('div');
const slotRange = document.createElement('input');
const startButton = document.createElement('button');

ringOptions.classList.add('ringOptions');
slotRange.classList.add('slotRange');
slotRange.value = '6';
startButton.classList.add('startButton');
startButton.innerHTML = 'Старт';

export {wrapperGame,gameField,wrapAllRings,wrapButtonGame,wrapBets,titleBet,bet50,bet100,wrapBalance,titleBalance,balanceBtn};
export {wrapRing1,ring1,diamond1,clover1,seven1,horseshoe1,chip1,card1,wrapRing2,ring2,diamond2,clover2,seven2,horseshoe2,chip2,card2};
export {wrapRing3,ring3,diamond3,clover3,seven3,horseshoe3,chip3,card3,ringOptions,slotRange,startButton};