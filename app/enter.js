//Enter

const wrapFormEnter = document.createElement('div');
const formEnter = document.createElement('form');
const crossFormEnter = document.createElement('span');
const titleformEnter = document.createElement('h1');
const loginEnter = document.createElement('input');

const wrapPasswordEnter = document.createElement('div');
const passwordEnter = document.createElement('input');
const eyeButtonEnter = document.createElement('a');

const inputFormEnter = document.createElement('input');
const recetFormEnter = document.createElement('button');

// Enter abilities

wrapFormEnter.classList.add('wrapFormEnter');
formEnter.classList.add('formEnter');
crossFormEnter.classList.add('crossFormEnter');
crossFormEnter.innerHTML='&times';
titleformEnter.innerText='Вход';

loginEnter.setAttribute('type','text');
loginEnter.setAttribute('placeholder','Введите логин');
loginEnter.classList.add('loginEnter','controlLogEnter');
loginEnter.setAttribute('required','true');

wrapPasswordEnter.classList.add('wrapPasswordEnter');
passwordEnter.setAttribute('type','password');
passwordEnter.setAttribute('placeholder','Введите пароль');
passwordEnter.classList.add('loginEnter','pasEnter');
passwordEnter.setAttribute('required','true');
eyeButtonEnter.classList.add('eyeButton');

inputFormEnter.setAttribute('type','submit');
inputFormEnter.value = 'Войти';
inputFormEnter.classList.add('inputFormEnter');

recetFormEnter.setAttribute('type','reset');
recetFormEnter.innerText='Очистить';
recetFormEnter.classList.add('recetFormEnter');

//Control login and password enter

const errorLogEnter = document.createElement('span');
errorLogEnter.classList.add('errorLogEnter');
errorLogEnter.innerHTML = 'Поле должно содержать от 5 до 20 букв или цифр';

export const controlLoginEnter = () => {
	const controlLogEnter = document.querySelector('.controlLogEnter').value;
	if((controlLogEnter.length < 5||controlLogEnter.length > 15)||(/^[a-zA-Z1-яА-Я0-9]+$/.test(controlLogEnter) === false)){
		errorLogEnter.style.display = 'block';
		loginEnter.style.border = '1px solid red';
		inputFormEnter.setAttribute('disabled','true');
	}
	 else{
		errorLogEnter.style.display = 'none';
		loginEnter.style.border = 'none';
		inputFormEnter.removeAttribute('disabled','true');
	}
};
const errorPasEnter = document.createElement('span');
errorPasEnter.classList.add('errorPasEnter');
errorPasEnter.innerHTML = 'Введите от 5 до 20 букв или цифр';

export const controlPasEnter = () => {
	const pasEnter = document.querySelector('.pasEnter').value;
	if ((pasEnter.length<5||pasEnter.length>20)||(/^[a-zA-Z1-яА-Я0-9]+$/.test(pasEnter) === false)){
		errorPasEnter.style.display = 'block';
		passwordEnter.style.border = '1px solid red';
		inputFormEnter.setAttribute('disabled','true');
	}
	else{
		errorPasEnter.style.display = 'none';
		passwordEnter.style.border = 'none';
		inputFormEnter.removeAttribute('disabled','true');
	}
};
const noFindPasEnter = document.createElement('span');
noFindPasEnter.classList.add('noFindPasEnter');
noFindPasEnter.innerHTML = 'Пароль не верен';

const noFindLogEnter = document.createElement('span');
noFindLogEnter.classList.add('noFindLogEnter');
noFindLogEnter.innerHTML = 'Пользователь не найден';

export {wrapFormEnter,formEnter,crossFormEnter,titleformEnter,loginEnter,wrapPasswordEnter,passwordEnter,eyeButtonEnter}
export {inputFormEnter,recetFormEnter,errorLogEnter,errorPasEnter,noFindPasEnter,noFindLogEnter};