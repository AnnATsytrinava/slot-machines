import {headerElement,insideHeader,logo,navigationHeader,inputEnter,inputRegistration} from './app/header.js';
import {wrapFormReg,formReg,crossForm,titleformReg,loginReg,passwordReg,wrapPassword,eyeButton} from './app/registration.js';
import {wrapPasswordRepeat,repeatPasswordReg,eyeButtonRepeat,mesPas,inputFormReg,recetFormReg,errorLogLength,errorSimilarLogin,errorPasLength} from './app/registration.js';
import {eyeWork,eyeWorkRepeat,controlLoginLength,controlPasLength,controlPasLast} from './app/registration.js';

import {wrapFormEnter,formEnter,crossFormEnter,titleformEnter,loginEnter,wrapPasswordEnter,passwordEnter,eyeButtonEnter,inputFormEnter,recetFormEnter} from './app/enter.js';
import {errorLogEnter,controlLoginEnter,errorPasEnter,controlPasEnter,noFindPasEnter,noFindLogEnter} from './app/enter.js';

import {wrapRule,ruleField,crossRule,titleRuleField,wrapCloverRule,cloverRuleImg,betCloverRule,wrapCardRule,cardRuleImg,betCardRule,wrapChipRule,chipRuleImg} from './app/ruleGame.js';
import {betChipRule,wrapHorseshoeRule,horseshoeRuleImg,betHorseshoeRule,wrapDiamondRule,diamondRuleImg,betDiamondRule,wrapSevenRule,sevenRuleImg,betSevenRule} from './app/ruleGame.js';

import {wrapperGame,gameField,wrapAllRings,wrapButtonGame,wrapBets,titleBet,bet50,bet100,wrapBalance,titleBalance,balanceBtn} from './app/ring.js';
import {wrapRing1,ring1,diamond1,clover1,seven1,horseshoe1,chip1,card1,wrapRing2,ring2,diamond2,clover2,seven2,horseshoe2,chip2,card2,} from './app/ring.js';
import {wrapRing3,ring3,diamond3,clover3,seven3,horseshoe3,chip3,card3,ringOptions,slotRange,startButton} from './app/ring.js';

const root = document.querySelector('#root');

//Header

root.appendChild(headerElement).appendChild(insideHeader).append(logo,navigationHeader);
navigationHeader.append(inputRegistration,inputEnter);

//Main

const main = document.createElement('main');

fetch('https://free4kwallpapers.com/uploads/originals/2019/10/12/minimalistic-city-skyline-wallpaper.jpg',{ mode: 'no-cors'})
.then(response => response.json())
.then(data => main.style.backgroundImage = data)
.catch(() => main.style.backgroundImage='url(img/backgroundMain.jpg)');


root.appendChild(main);

// Registration form

main.appendChild(wrapFormReg).appendChild(formReg).append(crossForm,titleformReg,loginReg,wrapPassword,wrapPasswordRepeat,inputFormReg,recetFormReg);
wrapPassword.append(passwordReg,eyeButton);
wrapPasswordRepeat.append(repeatPasswordReg,eyeButtonRepeat,mesPas);

//Eyes registration

eyeButton.addEventListener('click',eyeWork );
eyeButtonRepeat.addEventListener('click',eyeWorkRepeat);

//Control login registration

formReg.append(errorLogLength)
loginReg.addEventListener('keyup',controlLoginLength);

//Control password registration

wrapPassword.append(errorPasLength);

passwordReg.addEventListener('keyup',controlPasLength);

inputFormReg.addEventListener('click',controlPasLast);
repeatPasswordReg.addEventListener('keyup',controlPasLast);

recetFormReg.addEventListener('click', () => {
	errorLogLength.style.display = 'none';
	errorSimilarLogin.style.display = 'none';
	errorPasLength.style.display = 'none';
	mesPas.style.display = 'none';
	loginReg.style.border = 'none';
    passwordReg.style.border = 'none';
    repeatPasswordReg.style.border = 'none';
});

//Final window registration

const clearEvent = (e) => {
	document.querySelector('h1').innerHTML = 'Регистрация завершена!';
	localStorage.setItem(loginReg.value, passwordReg.value);
	loginReg.style.display = 'none';
    wrapPassword.style.display = 'none';
    eyeButton.style.display = 'none';
    wrapPasswordRepeat.style.display = 'none';
    eyeButtonRepeat.style.display = 'none';
    recetFormReg.style.display = 'none';
	inputFormReg.value = 'Войти';

    inputFormReg.addEventListener('click', () => {
	    wrapFormEnter.style.display = 'block';
	    document.querySelector('.wrapFormReg').style.display = 'none';
	});
	e.preventDefault();
};

formReg.addEventListener('submit',clearEvent);

const openForm = () => {
	document.querySelector('.wrapFormReg').style.display = 'block';
	document.querySelector('.wrapFormEnter').style.display = 'none';
};
const closeForm = () => {
	document.querySelector('.wrapFormReg').style.display = 'none';
};

document.querySelector('.buttonHeader').addEventListener('click', openForm);
document.querySelector('.crossForm').addEventListener('click', closeForm);

//Enter

main.appendChild(wrapFormEnter).appendChild(formEnter).append(crossFormEnter,titleformEnter,loginEnter,wrapPasswordEnter,inputFormEnter,recetFormEnter);
wrapPasswordEnter.append(passwordEnter,eyeButtonEnter);

//Open and eyes  enter

document.querySelector('.buttonHeaderEnter').addEventListener('click', () => {
	wrapFormEnter.style.display = 'block';
	document.querySelector('.wrapFormReg').style.display = 'none';
});

crossFormEnter.addEventListener('click', () => {
	wrapFormEnter.style.display = 'none';
});

eyeButtonEnter.addEventListener('click', () => {
	if (passwordEnter.getAttribute('type') == 'password') {
	   eyeButtonEnter.style.backgroundImage = 'url(img/iconsEye.png)';
	   passwordEnter.setAttribute('type','text');
	}
	else {
	   eyeButtonEnter.style.backgroundImage = 'url(img/iconsInvisibleEye.png)';
	   passwordEnter.setAttribute('type','password');
	}
});

//Control login enter

formEnter.append(errorLogEnter);

loginEnter.addEventListener('keyup',controlLoginEnter);

//Control password enter

wrapPasswordEnter.append(errorPasEnter);
passwordEnter.addEventListener('keyup',controlPasEnter);

//Reset Enter

recetFormEnter.addEventListener('click', () => {
	errorLogEnter.style.display = 'none';
	errorPasEnter.style.display = 'none';
	noFindPasEnter.style.display = 'none';
	noFindLogEnter.style.display = 'none';
	loginEnter.style.border = 'none';
    passwordEnter.style.border = 'none';
});

// Open game

const createGame = () => {
	wrapFormEnter.style.display = 'none';
	document.querySelector('.inputRegistration').style.display = 'none';

	//Open header game

	const ruleAuthorMenu = document.createElement('div');
	const ruleLink = document.createElement('a');
	const welcome = document.createElement('div');

	ruleAuthorMenu.classList.add('ruleAuthorMenu');
	ruleLink.classList.add('ruleLink');
	ruleLink.innerHTML = ('Правила');
	welcome.classList.add('welcome');
	welcome.innerHTML = ('Удачи, ' + loginEnter.value + ' !');
	document.querySelector('.navHeader').before(ruleAuthorMenu);
	ruleAuthorMenu.append(ruleLink,welcome);

	//Rule

	main.appendChild(wrapRule).appendChild(ruleField).append(crossRule,titleRuleField,wrapCloverRule,wrapCardRule,wrapChipRule,wrapHorseshoeRule,wrapDiamondRule,wrapSevenRule);
	wrapCloverRule.append(cloverRuleImg,betCloverRule);
	wrapCardRule.append(cardRuleImg,betCardRule);
	wrapChipRule.append(chipRuleImg,betChipRule);
	wrapHorseshoeRule.append(horseshoeRuleImg,betHorseshoeRule);
	wrapDiamondRule.append( diamondRuleImg,betDiamondRule);
	wrapSevenRule.append(sevenRuleImg,betSevenRule);

	ruleLink.addEventListener('click', () => {
		wrapRule.style.display = 'block';
		wrapperGame.style.display = 'none';
	})
	crossRule.addEventListener('click', () => {
		wrapRule.style.display = 'none';
		wrapperGame.style.display = 'block';
	})

	//Open game

	gameField.appendChild(wrapButtonGame);
	wrapButtonGame.append(wrapBets,wrapBalance);
	wrapBets.append(titleBet,bet50,bet100);
	wrapBalance.append(titleBalance,balanceBtn);

	bet100.addEventListener('click', () => {
		bet100.style.backgroundImage = 'url(img/betBoard.jpg)';
		bet100.style.cursor = 'default';
		bet100.classList.add('bet');

		bet50.style.backgroundImage = 'url(img/silverBetBoard.jpg)';
		bet50.style.cursor = 'pointer';
		bet50.classList.remove('bet');
	});
	bet50.addEventListener('click', () => {
		bet50.style.backgroundImage = 'url(img/betBoard.jpg)';
		bet50.style.cursor = 'default';
		bet50.classList.add('bet');

		bet100.style.backgroundImage = 'url(img/silverBetBoard.jpg)';
		bet100.style.cursor = 'pointer';
		bet100.classList.remove('bet');
	})

	// Ring 1
	
	wrapRing1.appendChild(ring1).append(diamond1,clover1,seven1,horseshoe1,chip1,card1);

	//Ring 2

	wrapRing2.appendChild(ring2).append(diamond2,clover2,seven2,horseshoe2,chip2,card2);

	//Ring 3

	wrapRing3.appendChild(ring3).append(diamond3,clover3,seven3,horseshoe3,chip3,card3);
	wrapAllRings.append(wrapRing1,wrapRing2,wrapRing3);
	
	ringOptions.append(slotRange,startButton);
	main.appendChild(wrapperGame).appendChild(gameField).append(wrapAllRings,ringOptions);

	const cells = ring1.querySelectorAll('.ring1Slot');
	const cells2 = ring2.querySelectorAll('.ring2Slot');
	const cells3 = ring3.querySelectorAll('.ring3Slot');

	let selectedIndex = 0;
	const rotateFn = 'rotateX';
	const slotCount = slotRange.value;
	const theta = 360 / slotCount;
	
	const cellHeight = ring1.offsetHeight;
	const cellHeight2 = ring2.offsetHeight;
	const cellHeight3 = ring3.offsetHeight;
	
	//Counts points

	const middleElements = () => {
		const middleElemRing1 = document.elementFromPoint(wrapperGame.offsetLeft-150 ,  10 + wrapperGame.offsetTop);
		const middleElemRing2 = document.elementFromPoint(80 + wrapperGame.offsetLeft ,  10 + wrapperGame.offsetTop);
		const middleElemRing3 = document.elementFromPoint( 135 + wrapperGame.offsetLeft, 10 + wrapperGame.offsetTop);
		const showBoxShadow = () => {
			document.querySelector('.wrapRing1').style.boxShadow = '0 0 .1rem #fff,inset 0 0 .1rem #fff,0 0 .5rem #08f,inset 0 0 .5rem #08f,0 0 .5rem #08f,inset 0 0 .5rem #08f';
			document.querySelector('.wrapRing2').style.boxShadow = '0 0 .1rem #fff,inset 0 0 .1rem #fff,0 0 .5rem #08f,inset 0 0 .5rem #08f,0 0 .5rem #08f,inset 0 0 .5rem #08f';
			document.querySelector('.wrapRing3').style.boxShadow = '0 0 .1rem #fff,inset 0 0 .1rem #fff,0 0 .5rem #08f,inset 0 0 .5rem #08f,0 0 .5rem #08f,inset 0 0 .5rem #08f';
			document.querySelector('.startButton').setAttribute('disabled','true')
		};
		const outBoxShadowRing = () => {
			document.querySelector('.wrapRing1').style.boxShadow = 'none';
			document.querySelector('.wrapRing2').style.boxShadow = 'none';
			document.querySelector('.wrapRing3').style.boxShadow = 'none';
			document.querySelector('.startButton').removeAttribute('disabled','true')
		};
		if(middleElemRing3.classList.contains('diamond')&&middleElemRing2.classList.contains('diamond')&&middleElemRing1.classList.contains('diamond')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 250;
			}
			if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 500;
			}
		}
		if(middleElemRing3.classList.contains('clover')&&middleElemRing2.classList.contains('clover')&&middleElemRing1.classList.contains('clover')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 50;
			}
			if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 100;
			}
		}
		if(middleElemRing3.classList.contains('seven')&&middleElemRing2.classList.contains('seven')&&middleElemRing1.classList.contains('seven')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 300;
			  }
			  if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 600;
			  }
		}
		if(middleElemRing3.classList.contains('horseshoe')&&middleElemRing2.classList.contains('horseshoe')&&middleElemRing1.classList.contains('horseshoe')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 200;
			  }
			if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 400;
			}
		}
		if(middleElemRing3.classList.contains('chip')&&middleElemRing2.classList.contains('chip')&&middleElemRing1.classList.contains('chip')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 150;
			}
			if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 300;
			}
		}
		if(middleElemRing3.classList.contains('card')&&middleElemRing2.classList.contains('card')&&middleElemRing1.classList.contains('card')){
			showBoxShadow();
			setTimeout(outBoxShadowRing, 500);
			if(document.querySelector('.bet').value==50){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 100;
			  }
			if(document.querySelector('.bet').value==100){
				balanceBtn.value = +document.querySelector('.balanceBtn').value + 200;
			}
		}
		localStorage.loggedIn = JSON.stringify({name:loginEnter.value, balance:balanceBtn.value});
	};

	// Spin rings

    const randomInteger = (min, max) => {
		const rand = min + Math.random() * (max + 1 - min);
		return Math.floor(rand);
	};
	const rotateRing = () => {
		const angle = theta * selectedIndex * (randomInteger(-7, -18));
		ring1.style.transform = 'translateZ(' + -radius + 'px) ' + rotateFn + '(' + angle + 'deg)';
	};
	const rotateRing2 = () => {
		const angle2 = theta * selectedIndex * (randomInteger(-10, -25));
		ring2.style.transform = 'translateZ(' + -radius + 'px) ' + rotateFn + '(' + angle2 + 'deg)';
	};
	const rotateRing3 = () => {
		const angle3 = theta * selectedIndex * (randomInteger(-10, -25));
		ring3.style.transform = 'translateZ(' + -radius + 'px) ' + rotateFn + '(' + angle3 + 'deg)';
	};

	startButton.addEventListener( 'click', () => {
		selectedIndex++;
		rotateRing();
		rotateRing2();
		rotateRing3();

		if (bet50.classList.contains('bet')){
			balanceBtn.value = balanceBtn.value-bet50.value;
			if(balanceBtn.value<50) {
				startButton.setAttribute('disabled','true');
				startButton.style.cursor = 'default';
				startButton.style.backgroundImage = 'url(img/silverSpinBtn.jpg)';
			}
		}
		if (bet100.classList.contains('bet')){
			balanceBtn.value = balanceBtn.value-bet100.value;
			if(balanceBtn.value<100) {
				startButton.setAttribute('disabled','true');
				startButton.style.cursor = 'default';
				startButton.style.backgroundImage = 'url(img/silverSpinBtn.jpg)';

				bet50.addEventListener( 'click', () => {
					startButton.removeAttribute('disabled','true');
					startButton.style.cursor = 'pointer';
					startButton.style.backgroundImage = 'url(img/spinBtn.png)';
					
					if(balanceBtn.value<50) {
						startButton.setAttribute('disabled','true');
						startButton.style.cursor = 'default';
						startButton.style.backgroundImage = 'url(img/silverSpinBtn.jpg)';
					}
				})
			}
		}
	});

	const cellSize = cellHeight;
	const cellSize2 = cellHeight2;
	const cellSize3 = cellHeight3;
	
	const  radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / slotCount ) );
	const  radius2 = Math.round( ( cellSize2 / 2) / Math.tan( Math.PI / slotCount ) );
	const  radius3 = Math.round( ( cellSize3 / 2) / Math.tan( Math.PI / slotCount ) );

	const changeRing = () => {
		for ( let i=0; i < cells.length; i++ ) {
			let cell = cells[i];
			if ( i < slotCount ) {
				cell.style.opacity = 1;
				let cellAngle = theta * i;
				cell.style.transform = rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
			}
			else {
				cell.style.opacity = 0;
				cell.style.transform = 'none';
			}
		}
		rotateRing();
	}
	changeRing();

	const changeRing2 = () => {
		for ( let i=0; i < cells.length; i++ ) {
			let cell2 = cells2[i];
			if ( i < slotCount ) {
				cell2.style.opacity = 1;
				let cellAngle = theta * i;
				cell2.style.transform = rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius2 + 'px)';
			}
			else {
				cell2.style.opacity = 0;
				cell2.style.transform = 'none';
			}
		}
		rotateRing2();
	}
	changeRing2();

	const changeRing3 = () => {
		for ( let i=0; i < cells.length; i++ ) {
			let cell3 = cells3[i];
			if ( i < slotCount ) {
				cell3.style.opacity = 1;
				let cellAngle = theta * i;
				cell3.style.transform = rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius3 + 'px)';
			}
			else {
				cell3.style.opacity = 0;
				cell3.style.transform = 'none';
			}
		}
		rotateRing3();
	}
	changeRing3();


	ring1.addEventListener('transitionend',middleElements);
	//ring2.addEventListener('transitionend',middleElements);
	//ring3.addEventListener('transitionend',middleElements);

	 //Change header back

	document.querySelector('.buttonHeaderEnter').value = 'Выйти';
	document.querySelector('.buttonHeaderEnter').addEventListener('click', () => {
		window.location.reload();
		localStorage.removeItem('loggedIn');
	});

	document.querySelector('.buttonHeaderEnter').addEventListener('click', () =>{
		loginEnter.value = '';
		passwordEnter.value = '';
		wrapperGame.style.display = 'none';
		ruleAuthorMenu.style.display = 'none';
		document.querySelector('.inputRegistration').style.display = 'block';
		document.querySelector('.buttonHeaderEnter').value = 'Войти';
		});
};

formEnter.addEventListener('submit', (e) => {
	e.preventDefault();
	if(localStorage.hasOwnProperty(loginEnter.value)){
		if(localStorage.getItem(loginEnter.value) == passwordEnter.value){
			createGame();
		}
		else {
			noFindPasEnter.style.display = 'block';

			wrapPasswordEnter.append(noFindPasEnter);

			document.querySelector('.pasEnter').style.border = '1px solid red';
			document.querySelector('.wrapPasswordEnter').addEventListener('click', () => {
		        noFindPasEnter.style.display = 'none';
		        document.querySelector('.pasEnter').style.border = 'none';
		    })
		}
    }

	//Not found in local storage

	else{
		noFindLogEnter.style.display = 'block';
		document.querySelector('.controlLogEnter').style.border = '1px solid red';

		formEnter.append(noFindLogEnter);

		document.querySelector('.formEnter').addEventListener('click', () => {
		    noFindLogEnter.style.display = 'none';
		    document.querySelector('.controlLogEnter').style.border = 'none';
		})
	}
});

const openSavePage = () => {
	if(localStorage.hasOwnProperty('loggedIn')){
		createGame();
		let loggedIn = JSON.parse( localStorage.loggedIn);
		//document.querySelector('.welcome').innerHTML = ('Удачи, ' + loggedIn.name  + ' !');
		document.querySelector('.welcome').innerHTML = ('');
		balanceBtn.value = loggedIn.balance;
	}
};
window.addEventListener('load', openSavePage);
